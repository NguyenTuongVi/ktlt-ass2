#ifndef DOIMATKHAU_H
#define DOIMATKHAU_H

#include <QWidget>

namespace Ui {
class DoiMatKhau;
}

class DoiMatKhau : public QWidget
{
    Q_OBJECT

public:
    explicit DoiMatKhau(QWidget *parent = 0);
    ~DoiMatKhau();

private:
    Ui::DoiMatKhau *ui;
};

#endif // DOIMATKHAU_H
