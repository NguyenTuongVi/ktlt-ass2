/********************************************************************************
** Form generated from reading UI file 'dangky.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DANGKY_H
#define UI_DANGKY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DangKy
{
public:
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *line_hten;
    QLineEdit *line_cmnd;
    QLineEdit *line_ns;
    QLineEdit *line_ngnghiep;
    QLineEdit *line_email;
    QToolButton *toolButton;
    QLineEdit *line_mk;
    QLineEdit *line_ttk;
    QLabel *label_7;
    QLabel *label_8;
    QGroupBox *groupBox;
    QLabel *label;
    QLabel *label_9;
    QLineEdit *line_mk_2;
    QLabel *lbl_note;

    void setupUi(QWidget *DangKy)
    {
        if (DangKy->objectName().isEmpty())
            DangKy->setObjectName(QStringLiteral("DangKy"));
        DangKy->resize(540, 331);
        DangKy->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        label_2 = new QLabel(DangKy);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 180, 101, 16));
        label_2->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_3 = new QLabel(DangKy);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 210, 111, 16));
        label_3->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_4 = new QLabel(DangKy);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 240, 121, 16));
        label_4->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_5 = new QLabel(DangKy);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 270, 111, 16));
        label_5->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_6 = new QLabel(DangKy);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(20, 300, 111, 16));
        label_6->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        line_hten = new QLineEdit(DangKy);
        line_hten->setObjectName(QStringLiteral("line_hten"));
        line_hten->setGeometry(QRect(150, 180, 113, 20));
        line_cmnd = new QLineEdit(DangKy);
        line_cmnd->setObjectName(QStringLiteral("line_cmnd"));
        line_cmnd->setGeometry(QRect(150, 210, 113, 20));
        line_ns = new QLineEdit(DangKy);
        line_ns->setObjectName(QStringLiteral("line_ns"));
        line_ns->setGeometry(QRect(150, 240, 161, 20));
        line_ngnghiep = new QLineEdit(DangKy);
        line_ngnghiep->setObjectName(QStringLiteral("line_ngnghiep"));
        line_ngnghiep->setGeometry(QRect(150, 270, 161, 20));
        line_email = new QLineEdit(DangKy);
        line_email->setObjectName(QStringLiteral("line_email"));
        line_email->setGeometry(QRect(150, 300, 161, 20));
        toolButton = new QToolButton(DangKy);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(430, 290, 101, 31));
        toolButton->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 170, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"\n"
"font: 75 16pt \"Times New Roman\";"));
        line_mk = new QLineEdit(DangKy);
        line_mk->setObjectName(QStringLiteral("line_mk"));
        line_mk->setGeometry(QRect(150, 120, 113, 20));
        line_ttk = new QLineEdit(DangKy);
        line_ttk->setObjectName(QStringLiteral("line_ttk"));
        line_ttk->setGeometry(QRect(150, 90, 113, 20));
        label_7 = new QLabel(DangKy);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(20, 90, 121, 20));
        label_7->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_8 = new QLabel(DangKy);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 120, 71, 20));
        label_8->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        groupBox = new QGroupBox(DangKy);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 541, 81));
        groupBox->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 85, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 261, 41));
        QFont font;
        font.setFamily(QStringLiteral(".VnArial"));
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        label_9 = new QLabel(DangKy);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(20, 150, 111, 16));
        label_9->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        line_mk_2 = new QLineEdit(DangKy);
        line_mk_2->setObjectName(QStringLiteral("line_mk_2"));
        line_mk_2->setGeometry(QRect(150, 150, 113, 20));
        lbl_note = new QLabel(DangKy);
        lbl_note->setObjectName(QStringLiteral("lbl_note"));
        lbl_note->setGeometry(QRect(340, 100, 191, 101));
        QFont font1;
        font1.setFamily(QStringLiteral("Times New Roman"));
        font1.setPointSize(10);
        font1.setBold(false);
        font1.setItalic(true);
        font1.setWeight(50);
        lbl_note->setFont(font1);
        lbl_note->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"font: italic 10pt \"Times New Roman\";"));
        lbl_note->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        retranslateUi(DangKy);

        QMetaObject::connectSlotsByName(DangKy);
    } // setupUi

    void retranslateUi(QWidget *DangKy)
    {
        DangKy->setWindowTitle(QApplication::translate("DangKy", "\304\220\304\203ng k\303\275", Q_NULLPTR));
        label_2->setText(QApplication::translate("DangKy", "H\341\273\215 v\303\240 t\303\252n", Q_NULLPTR));
        label_3->setText(QApplication::translate("DangKy", "CMND ho\341\272\267c MSSV", Q_NULLPTR));
        label_4->setText(QApplication::translate("DangKy", "Ng\303\240y,th\303\241ng,n\304\203m sinh", Q_NULLPTR));
        label_5->setText(QApplication::translate("DangKy", "Ngh\341\273\201 nghi\341\273\207p", Q_NULLPTR));
        label_6->setText(QApplication::translate("DangKy", "Email", Q_NULLPTR));
        toolButton->setText(QApplication::translate("DangKy", "\304\220\304\203ng k\303\275", Q_NULLPTR));
        label_7->setText(QApplication::translate("DangKy", "T\303\252n t\303\240i kho\341\272\243n", Q_NULLPTR));
        label_8->setText(QApplication::translate("DangKy", "M\341\272\255t kh\341\272\251u", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("DangKy", "LIBPRO", Q_NULLPTR));
        label->setText(QApplication::translate("DangKy", "<html><head/><body><p>REGISTER<span style=\" font-style:italic;\"/><span style=\" font-style:italic; vertical-align:sub;\">FOR LIBPRO</span></p></body></html>", Q_NULLPTR));
        label_9->setText(QApplication::translate("DangKy", "Nh\341\272\255p l\341\272\241i m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        lbl_note->setText(QApplication::translate("DangKy", "Note:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DangKy: public Ui_DangKy {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DANGKY_H
