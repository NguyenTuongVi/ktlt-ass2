/********************************************************************************
** Form generated from reading UI file 'doimatkhau.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOIMATKHAU_H
#define UI_DOIMATKHAU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DoiMatKhau
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *line_ttk;
    QLineEdit *line_mkc;
    QLineEdit *line_mkm;
    QLineEdit *line_nhaplai;
    QPushButton *pushButton;
    QLabel *lbl_note;

    void setupUi(QWidget *DoiMatKhau)
    {
        if (DoiMatKhau->objectName().isEmpty())
            DoiMatKhau->setObjectName(QStringLiteral("DoiMatKhau"));
        DoiMatKhau->resize(400, 216);
        groupBox = new QGroupBox(DoiMatKhau);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 401, 221));
        groupBox->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 14pt \".VnArabia\";"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 50, 81, 21));
        label->setStyleSheet(QStringLiteral("font: 75 12pt \"Times New Roman\";"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 80, 81, 16));
        label_2->setStyleSheet(QStringLiteral("font: 75 10pt \"Times New Roman\";"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 110, 71, 16));
        label_3->setStyleSheet(QStringLiteral("font: 75 10pt \"Times New Roman\";"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 140, 81, 16));
        label_4->setStyleSheet(QStringLiteral("font: 75 10pt \"Times New Roman\";"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 170, 131, 16));
        label_5->setStyleSheet(QStringLiteral("font: 75 10pt \"Times New Roman\";"));
        line_ttk = new QLineEdit(groupBox);
        line_ttk->setObjectName(QStringLiteral("line_ttk"));
        line_ttk->setGeometry(QRect(110, 80, 113, 20));
        line_ttk->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 0);\n"
"font: 75 10pt \"Times New Roman\";"));
        line_mkc = new QLineEdit(groupBox);
        line_mkc->setObjectName(QStringLiteral("line_mkc"));
        line_mkc->setGeometry(QRect(110, 110, 113, 20));
        line_mkc->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 0);\n"
"font: 75 10pt \"Times New Roman\";"));
        line_mkm = new QLineEdit(groupBox);
        line_mkm->setObjectName(QStringLiteral("line_mkm"));
        line_mkm->setGeometry(QRect(110, 140, 113, 20));
        line_mkm->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 0);\n"
"font: 75 10pt \"Times New Roman\";"));
        line_nhaplai = new QLineEdit(groupBox);
        line_nhaplai->setObjectName(QStringLiteral("line_nhaplai"));
        line_nhaplai->setGeometry(QRect(150, 170, 113, 20));
        line_nhaplai->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 0);\n"
"font: 75 10pt \"Times New Roman\";"));
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(300, 180, 75, 23));
        pushButton->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 170, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 75 10pt \"Times New Roman\";"));
        lbl_note = new QLabel(groupBox);
        lbl_note->setObjectName(QStringLiteral("lbl_note"));
        lbl_note->setGeometry(QRect(110, 50, 241, 16));
        lbl_note->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"font: italic 10pt \"Times New Roman\";"));

        retranslateUi(DoiMatKhau);

        QMetaObject::connectSlotsByName(DoiMatKhau);
    } // setupUi

    void retranslateUi(QWidget *DoiMatKhau)
    {
        DoiMatKhau->setWindowTitle(QApplication::translate("DoiMatKhau", "\304\220\341\273\225i M\341\272\255t Kh\341\272\251u", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("DoiMatKhau", " LIBPRO", Q_NULLPTR));
        label->setText(QApplication::translate("DoiMatKhau", "\304\220\341\273\225i m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        label_2->setText(QApplication::translate("DoiMatKhau", "T\303\252n t\303\240i kho\341\272\243n", Q_NULLPTR));
        label_3->setText(QApplication::translate("DoiMatKhau", "M\341\272\255t kh\341\272\251u c\305\251", Q_NULLPTR));
        label_4->setText(QApplication::translate("DoiMatKhau", "M\341\272\255t kh\341\272\251u m\341\273\233i", Q_NULLPTR));
        label_5->setText(QApplication::translate("DoiMatKhau", "Nh\341\272\255p l\341\272\241i m\341\272\255t kh\341\272\251u m\341\273\233i", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DoiMatKhau", "OK", Q_NULLPTR));
        lbl_note->setText(QApplication::translate("DoiMatKhau", "Note", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DoiMatKhau: public Ui_DoiMatKhau {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOIMATKHAU_H
