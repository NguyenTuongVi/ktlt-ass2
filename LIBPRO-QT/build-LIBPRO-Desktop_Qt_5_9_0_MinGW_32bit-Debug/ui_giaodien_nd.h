/********************************************************************************
** Form generated from reading UI file 'giaodien_nd.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GIAODIEN_ND_H
#define UI_GIAODIEN_ND_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Giaodien_ND
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *line_search;
    QPushButton *btn_search;
    QPushButton *btn_dangnhap;
    QLabel *lbl_user;
    QPushButton *btn_doimatkhau;
    QPushButton *btn_thoat;
    QTabWidget *tabWidget;
    QWidget *tab_ND;
    QGroupBox *group_thongbao;
    QListWidget *list_thongbao;
    QGroupBox *groupBox_search;
    QListWidget *list_search;
    QGroupBox *groupBox_2;
    QTreeWidget *tree_sach;
    QWidget *tab_TTND;
    QGroupBox *groupBox_3;
    QTableWidget *tableWidget_2;
    QWidget *tab_QLS;
    QTableWidget *tableWidget;
    QGroupBox *groupBox_4;
    QWidget *tab_QLND;
    QGroupBox *groupBox_5;

    void setupUi(QWidget *Giaodien_ND)
    {
        if (Giaodien_ND->objectName().isEmpty())
            Giaodien_ND->setObjectName(QStringLiteral("Giaodien_ND"));
        Giaodien_ND->resize(690, 366);
        groupBox = new QGroupBox(Giaodien_ND);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 691, 51));
        groupBox->setStyleSheet(QLatin1String("background-color: rgb(85, 170, 255);\n"
"color: rgb(0, 0, 255);\n"
"font: 10pt \".VnArabia\";"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 61, 31));
        label->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 12pt \".VnArabia\";"));
        line_search = new QLineEdit(groupBox);
        line_search->setObjectName(QStringLiteral("line_search"));
        line_search->setGeometry(QRect(90, 10, 161, 31));
        line_search->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: italic 10pt \"Times New Roman\";\n"
"color: rgb(0, 0, 0);"));
        btn_search = new QPushButton(groupBox);
        btn_search->setObjectName(QStringLiteral("btn_search"));
        btn_search->setGeometry(QRect(250, 10, 31, 31));
        btn_search->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0.392, x2:1, y2:0, stop:0 rgba(255, 85, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        QIcon icon;
        icon.addFile(QStringLiteral("../icon/search-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_search->setIcon(icon);
        btn_dangnhap = new QPushButton(groupBox);
        btn_dangnhap->setObjectName(QStringLiteral("btn_dangnhap"));
        btn_dangnhap->setGeometry(QRect(610, 10, 71, 31));
        btn_dangnhap->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0.392, x2:1, y2:0, stop:0 rgba(255, 85, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        lbl_user = new QLabel(groupBox);
        lbl_user->setObjectName(QStringLiteral("lbl_user"));
        lbl_user->setGeometry(QRect(290, 10, 171, 31));
        lbl_user->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";"));
        btn_doimatkhau = new QPushButton(groupBox);
        btn_doimatkhau->setObjectName(QStringLiteral("btn_doimatkhau"));
        btn_doimatkhau->setGeometry(QRect(530, 10, 31, 31));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../icon/key.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        btn_doimatkhau->setIcon(icon1);
        btn_doimatkhau->setIconSize(QSize(25, 25));
        btn_thoat = new QPushButton(groupBox);
        btn_thoat->setObjectName(QStringLiteral("btn_thoat"));
        btn_thoat->setGeometry(QRect(570, 10, 31, 31));
        QIcon icon2;
        icon2.addFile(QStringLiteral("../icon/dx.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        btn_thoat->setIcon(icon2);
        btn_thoat->setIconSize(QSize(25, 25));
        tabWidget = new QTabWidget(Giaodien_ND);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setGeometry(QRect(0, 50, 691, 321));
        tabWidget->setStyleSheet(QLatin1String("font: 75 10pt \"Times New Roman\";\n"
"\n"
"color: rgb(0, 0, 255);"));
        tabWidget->setIconSize(QSize(16, 16));
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(false);
        tabWidget->setTabBarAutoHide(false);
        tab_ND = new QWidget();
        tab_ND->setObjectName(QStringLiteral("tab_ND"));
        group_thongbao = new QGroupBox(tab_ND);
        group_thongbao->setObjectName(QStringLiteral("group_thongbao"));
        group_thongbao->setGeometry(QRect(470, 0, 221, 271));
        group_thongbao->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"\n"
"font: 75 10pt \"Times New Roman\";"));
        list_thongbao = new QListWidget(group_thongbao);
        list_thongbao->setObjectName(QStringLiteral("list_thongbao"));
        list_thongbao->setGeometry(QRect(0, 20, 211, 251));
        list_thongbao->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 italic 10pt \"Times New Roman\";"));
        groupBox_search = new QGroupBox(tab_ND);
        groupBox_search->setObjectName(QStringLiteral("groupBox_search"));
        groupBox_search->setGeometry(QRect(160, 0, 311, 271));
        groupBox_search->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"\n"
"font: 75 10pt \"Times New Roman\";"));
        list_search = new QListWidget(groupBox_search);
        list_search->setObjectName(QStringLiteral("list_search"));
        list_search->setGeometry(QRect(0, 20, 311, 251));
        groupBox_2 = new QGroupBox(tab_ND);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 270, 691, 21));
        groupBox_2->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        tree_sach = new QTreeWidget(tab_ND);
        QIcon icon3;
        icon3.addFile(QStringLiteral("../icon/SGK.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setIcon(0, icon3);
        tree_sach->setHeaderItem(__qtreewidgetitem);
        QIcon icon4;
        icon4.addFile(QStringLiteral("../icon/Mac.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral("../icon/ly.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(tree_sach);
        __qtreewidgetitem1->setIcon(0, icon4);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(tree_sach);
        __qtreewidgetitem2->setIcon(0, icon5);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(tree_sach);
        __qtreewidgetitem3->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(tree_sach);
        __qtreewidgetitem4->setIcon(0, icon5);
        tree_sach->setObjectName(QStringLiteral("tree_sach"));
        tree_sach->setGeometry(QRect(0, 0, 161, 271));
        QIcon icon6;
        icon6.addFile(QStringLiteral("../icon/iconthuvien.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_ND, icon6, QString());
        tab_TTND = new QWidget();
        tab_TTND->setObjectName(QStringLiteral("tab_TTND"));
        groupBox_3 = new QGroupBox(tab_TTND);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(0, 270, 691, 21));
        groupBox_3->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        tableWidget_2 = new QTableWidget(tab_TTND);
        if (tableWidget_2->columnCount() < 5)
            tableWidget_2->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget_2->setObjectName(QStringLiteral("tableWidget_2"));
        tableWidget_2->setGeometry(QRect(0, 0, 511, 271));
        QIcon icon7;
        icon7.addFile(QStringLiteral("../icon/thongtin.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_TTND, icon7, QString());
        tab_QLS = new QWidget();
        tab_QLS->setObjectName(QStringLiteral("tab_QLS"));
        tableWidget = new QTableWidget(tab_QLS);
        if (tableWidget->columnCount() < 3)
            tableWidget->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem7);
        if (tableWidget->rowCount() < 1)
            tableWidget->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setItem(0, 1, __qtablewidgetitem9);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 411, 271));
        groupBox_4 = new QGroupBox(tab_QLS);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(0, 270, 691, 21));
        groupBox_4->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        tabWidget->addTab(tab_QLS, icon4, QString());
        tab_QLND = new QWidget();
        tab_QLND->setObjectName(QStringLiteral("tab_QLND"));
        groupBox_5 = new QGroupBox(tab_QLND);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(0, 270, 691, 21));
        groupBox_5->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        QIcon icon8;
        icon8.addFile(QStringLiteral("../icon/nd.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_QLND, icon8, QString());

        retranslateUi(Giaodien_ND);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Giaodien_ND);
    } // setupUi

    void retranslateUi(QWidget *Giaodien_ND)
    {
        Giaodien_ND->setWindowTitle(QApplication::translate("Giaodien_ND", "Form", Q_NULLPTR));
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("Giaodien_ND", "LIBPRO", Q_NULLPTR));
        line_search->setText(QApplication::translate("Giaodien_ND", "Search", Q_NULLPTR));
        btn_search->setText(QString());
        btn_dangnhap->setText(QApplication::translate("Giaodien_ND", "\304\220\304\203ng nh\341\272\255p", Q_NULLPTR));
        lbl_user->setText(QString());
        btn_doimatkhau->setText(QString());
        btn_thoat->setText(QString());
        group_thongbao->setTitle(QApplication::translate("Giaodien_ND", "Th\303\264ng b\303\241o", Q_NULLPTR));
        groupBox_search->setTitle(QApplication::translate("Giaodien_ND", "Result search...", Q_NULLPTR));
        groupBox_2->setTitle(QString());
        QTreeWidgetItem *___qtreewidgetitem = tree_sach->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("Giaodien_ND", "Th\306\260 vi\341\273\207n s\303\241ch", Q_NULLPTR));

        const bool __sortingEnabled = tree_sach->isSortingEnabled();
        tree_sach->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = tree_sach->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("Giaodien_ND", "\304\220\341\272\241i c\306\260\306\241ng", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(0, QApplication::translate("Giaodien_ND", "To\303\241n", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(0, QApplication::translate("Giaodien_ND", "L\303\275", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(0, QApplication::translate("Giaodien_ND", "Ho\303\241", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(0, QApplication::translate("Giaodien_ND", "Mac-Lenin", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem6 = tree_sach->topLevelItem(1);
        ___qtreewidgetitem6->setText(0, QApplication::translate("Giaodien_ND", "Chuy\303\252n ng\303\240nh", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(0, QApplication::translate("Giaodien_ND", "K\341\273\271 thu\341\272\255t l\341\272\255p tr\303\254nh", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem6->child(1);
        ___qtreewidgetitem8->setText(0, QApplication::translate("Giaodien_ND", "H\341\273\207 th\341\273\221ng s\341\273\221", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem6->child(2);
        ___qtreewidgetitem9->setText(0, QApplication::translate("Giaodien_ND", "C\341\272\245u tr\303\272c d\341\273\257 li\341\273\207u", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem6->child(3);
        ___qtreewidgetitem10->setText(0, QApplication::translate("Giaodien_ND", "Thi\341\272\277t k\341\272\277 gi\341\272\243i thu\341\272\255t", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem6->child(4);
        ___qtreewidgetitem11->setText(0, QApplication::translate("Giaodien_ND", "Ki\341\272\277n tr\303\272c m\303\241y t\303\255nh", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem6->child(5);
        ___qtreewidgetitem12->setText(0, QApplication::translate("Giaodien_ND", "PPL", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem6->child(6);
        ___qtreewidgetitem13->setText(0, QApplication::translate("Giaodien_ND", "M\341\272\241ng m\303\241y t\303\255nh", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem6->child(7);
        ___qtreewidgetitem14->setText(0, QApplication::translate("Giaodien_ND", "An ninh m\341\272\241ng", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem15 = tree_sach->topLevelItem(2);
        ___qtreewidgetitem15->setText(0, QApplication::translate("Giaodien_ND", "S\303\241ch tham kh\341\272\243o", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem16 = tree_sach->topLevelItem(3);
        ___qtreewidgetitem16->setText(0, QApplication::translate("Giaodien_ND", "S\303\241ch khoa h\341\273\215c", Q_NULLPTR));
        tree_sach->setSortingEnabled(__sortingEnabled);

        tabWidget->setTabText(tabWidget->indexOf(tab_ND), QApplication::translate("Giaodien_ND", "LIBPRO", Q_NULLPTR));
        groupBox_3->setTitle(QString());
        QTableWidgetItem *___qtablewidgetitem = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("Giaodien_ND", "S\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("Giaodien_ND", "New Column", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("Giaodien_ND", "Ng\303\240y m\306\260\341\273\243n", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("Giaodien_ND", "Ng\303\240y tr\341\272\243", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("Giaodien_ND", "Ti\341\273\201n ph\341\272\241t", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_TTND), QApplication::translate("Giaodien_ND", "Th\303\264ng tin ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem5->setText(QApplication::translate("Giaodien_ND", "ID", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem6->setText(QApplication::translate("Giaodien_ND", "T\303\252n s\303\241ch", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem7->setText(QApplication::translate("Giaodien_ND", "Ng\303\240y c\341\272\255p nh\341\272\255t", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem8->setText(QApplication::translate("Giaodien_ND", "1", Q_NULLPTR));

        const bool __sortingEnabled1 = tableWidget->isSortingEnabled();
        tableWidget->setSortingEnabled(false);
        tableWidget->setSortingEnabled(__sortingEnabled1);

        groupBox_4->setTitle(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_QLS), QApplication::translate("Giaodien_ND", "Qu\341\272\243n l\303\275 s\303\241ch", Q_NULLPTR));
        groupBox_5->setTitle(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_QLND), QApplication::translate("Giaodien_ND", "Qu\341\272\243n l\303\275 ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Giaodien_ND: public Ui_Giaodien_ND {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GIAODIEN_ND_H
