#ifndef DANGKY_H
#define DANGKY_H

#include <QWidget>

namespace Ui {
class DangKy;
}

class DangKy : public QWidget
{
    Q_OBJECT

public:
    explicit DangKy(QWidget *parent = 0);
    ~DangKy();

private:
    Ui::DangKy *ui;
};

#endif // DANGKY_H
