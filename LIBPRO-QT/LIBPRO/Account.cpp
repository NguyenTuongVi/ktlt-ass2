#include "Account.h"


Account::Account()
{
}
string Account::GetUsername()
{
	return this->Username;
}
void Account::SetUserName(string username)
{
	this->Username = username;
}
string Account::GetPassword()
{
	return this->Password;
}
void Account::SetPassword(string password)
{
	this->Password = password;
}
string  Account::GetID()
{
	return this->ID;
}
void Account::SetID(string id)
{
	this->ID = id;
}
bool Account::GetState()
{
	return this->State;
}
void Account::SetState(bool state)
{
	this->State = state;
}

Account::~Account()
{
}

