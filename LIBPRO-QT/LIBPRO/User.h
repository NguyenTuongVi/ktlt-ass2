#pragma once
#include <string>
#include <QDate>
using namespace std;
class User
{
public:
	User();

	~User();
    QDate Getbirthday();
    void Setbirtday(QDate);
	string Getmssv();
	void Setmssv(string);
	string GetName();
	void SetName(string);
	string GetID();
	void SetID(string);
	string GetCMND();
	void SetCMND(string);
	string Getemail();
	void Setemail(string);
	string Getjob();
	void Setjob(string);
private:
	string ID;
	string CMND;
	string MSSV;
	string Name;
    QDate Birthday;
	string email;
	string job;
};

