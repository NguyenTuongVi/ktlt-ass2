/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QToolButton *btn_login;
    QLabel *lbl_name;
    QLabel *lbl_note;
    QLabel *lbl_user;
    QLabel *lbl_pass;
    QLineEdit *line_user;
    QLineEdit *line_pass;
    QToolButton *btn_qmk;
    QToolButton *btn_dk;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 281);
        MainWindow->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral(""));
        btn_login = new QToolButton(centralWidget);
        btn_login->setObjectName(QStringLiteral("btn_login"));
        btn_login->setGeometry(QRect(230, 130, 81, 22));
        QFont font;
        font.setFamily(QStringLiteral(".VnArabia"));
        font.setPointSize(10);
        btn_login->setFont(font);
        btn_login->setAutoFillBackground(false);
        btn_login->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 10pt \".VnArabia\";\n"
"color: rgb(255, 255, 255);"));
        QIcon icon(QIcon::fromTheme(QStringLiteral("a")));
        btn_login->setIcon(icon);
        btn_login->setToolButtonStyle(Qt::ToolButtonIconOnly);
        lbl_name = new QLabel(centralWidget);
        lbl_name->setObjectName(QStringLiteral("lbl_name"));
        lbl_name->setGeometry(QRect(80, 10, 241, 61));
        QFont font1;
        font1.setFamily(QStringLiteral(".VnArabia"));
        font1.setPointSize(48);
        lbl_name->setFont(font1);
        lbl_name->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 48pt \".VnArabia\";"));
        lbl_name->setFrameShape(QFrame::NoFrame);
        lbl_name->setFrameShadow(QFrame::Plain);
        lbl_name->setTextFormat(Qt::AutoText);
        lbl_name->setAlignment(Qt::AlignCenter);
        lbl_note = new QLabel(centralWidget);
        lbl_note->setObjectName(QStringLiteral("lbl_note"));
        lbl_note->setGeometry(QRect(80, 70, 231, 20));
        lbl_note->setStyleSheet(QLatin1String("\n"
"color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";"));
        lbl_note->setAlignment(Qt::AlignCenter);
        lbl_user = new QLabel(centralWidget);
        lbl_user->setObjectName(QStringLiteral("lbl_user"));
        lbl_user->setGeometry(QRect(30, 100, 71, 21));
        lbl_user->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";\n"
""));
        lbl_pass = new QLabel(centralWidget);
        lbl_pass->setObjectName(QStringLiteral("lbl_pass"));
        lbl_pass->setGeometry(QRect(30, 130, 71, 21));
        lbl_pass->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";"));
        line_user = new QLineEdit(centralWidget);
        line_user->setObjectName(QStringLiteral("line_user"));
        line_user->setGeometry(QRect(110, 100, 113, 20));
        line_user->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        line_pass = new QLineEdit(centralWidget);
        line_pass->setObjectName(QStringLiteral("line_pass"));
        line_pass->setGeometry(QRect(110, 130, 113, 20));
        line_pass->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 255);"));
        btn_qmk = new QToolButton(centralWidget);
        btn_qmk->setObjectName(QStringLiteral("btn_qmk"));
        btn_qmk->setGeometry(QRect(30, 200, 81, 21));
        btn_qmk->setStyleSheet(QLatin1String("\n"
"font: 8pt \"MS Shell Dlg 2\";"));
        btn_dk = new QToolButton(centralWidget);
        btn_dk->setObjectName(QStringLiteral("btn_dk"));
        btn_dk->setGeometry(QRect(230, 200, 81, 19));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Login to LIBPRO", Q_NULLPTR));
#ifndef QT_NO_WHATSTHIS
        MainWindow->setWhatsThis(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
#endif // QT_NO_WHATSTHIS
        btn_login->setText(QApplication::translate("MainWindow", "Login", Q_NULLPTR));
        lbl_name->setText(QApplication::translate("MainWindow", "LIBPRO", Q_NULLPTR));
        lbl_note->setText(QApplication::translate("MainWindow", "Simple Library For Student", Q_NULLPTR));
        lbl_user->setText(QApplication::translate("MainWindow", "Username", Q_NULLPTR));
        lbl_pass->setText(QApplication::translate("MainWindow", "Password", Q_NULLPTR));
        btn_qmk->setText(QApplication::translate("MainWindow", "Qu\303\252n m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        btn_dk->setText(QApplication::translate("MainWindow", "\304\220\304\203ng k\303\275", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
