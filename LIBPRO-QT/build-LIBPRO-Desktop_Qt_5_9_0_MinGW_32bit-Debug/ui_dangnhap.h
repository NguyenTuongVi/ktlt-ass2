/********************************************************************************
** Form generated from reading UI file 'dangnhap.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DANGNHAP_H
#define UI_DANGNHAP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DangNhap
{
public:
    QWidget *centralWidget;
    QToolButton *btn_login;
    QLabel *lbl_name;
    QLabel *lbl_note;
    QLabel *lbl_user;
    QLabel *lbl_pass;
    QLineEdit *line_user;
    QLineEdit *line_pass;
    QToolButton *btn_qmk;
    QToolButton *btn_dk;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *DangNhap)
    {
        if (DangNhap->objectName().isEmpty())
            DangNhap->setObjectName(QStringLiteral("DangNhap"));
        DangNhap->resize(400, 281);
        DangNhap->setStyleSheet(QStringLiteral("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(85, 170, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        centralWidget = new QWidget(DangNhap);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral(""));
        btn_login = new QToolButton(centralWidget);
        btn_login->setObjectName(QStringLiteral("btn_login"));
        btn_login->setGeometry(QRect(230, 130, 81, 22));
        QFont font;
        font.setFamily(QStringLiteral("Times New Roman"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        btn_login->setFont(font);
        btn_login->setAutoFillBackground(false);
        btn_login->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 170, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 75 10pt \"Times New Roman\";\n"
"color: rgb(255, 255, 255);"));
        btn_login->setToolButtonStyle(Qt::ToolButtonIconOnly);
        lbl_name = new QLabel(centralWidget);
        lbl_name->setObjectName(QStringLiteral("lbl_name"));
        lbl_name->setGeometry(QRect(80, 10, 241, 61));
        QFont font1;
        font1.setFamily(QStringLiteral(".VnArabia"));
        font1.setPointSize(48);
        lbl_name->setFont(font1);
        lbl_name->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 48pt \".VnArabia\";"));
        lbl_name->setFrameShape(QFrame::NoFrame);
        lbl_name->setFrameShadow(QFrame::Plain);
        lbl_name->setTextFormat(Qt::AutoText);
        lbl_name->setAlignment(Qt::AlignCenter);
        lbl_note = new QLabel(centralWidget);
        lbl_note->setObjectName(QStringLiteral("lbl_note"));
        lbl_note->setGeometry(QRect(80, 70, 231, 20));
        lbl_note->setStyleSheet(QLatin1String("\n"
"color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";"));
        lbl_note->setAlignment(Qt::AlignCenter);
        lbl_user = new QLabel(centralWidget);
        lbl_user->setObjectName(QStringLiteral("lbl_user"));
        lbl_user->setGeometry(QRect(30, 100, 71, 21));
        lbl_user->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";\n"
""));
        lbl_pass = new QLabel(centralWidget);
        lbl_pass->setObjectName(QStringLiteral("lbl_pass"));
        lbl_pass->setGeometry(QRect(30, 130, 71, 21));
        lbl_pass->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"font: 10pt \".VnArabia\";"));
        line_user = new QLineEdit(centralWidget);
        line_user->setObjectName(QStringLiteral("line_user"));
        line_user->setGeometry(QRect(110, 100, 113, 20));
        line_user->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));"));
        line_pass = new QLineEdit(centralWidget);
        line_pass->setObjectName(QStringLiteral("line_pass"));
        line_pass->setGeometry(QRect(110, 130, 113, 20));
        line_pass->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(0, 0, 255);"));
        btn_qmk = new QToolButton(centralWidget);
        btn_qmk->setObjectName(QStringLiteral("btn_qmk"));
        btn_qmk->setGeometry(QRect(50, 200, 91, 21));
        btn_qmk->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0.392, x2:1, y2:0, stop:0 rgba(255, 85, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        btn_dk = new QToolButton(centralWidget);
        btn_dk->setObjectName(QStringLiteral("btn_dk"));
        btn_dk->setGeometry(QRect(210, 200, 101, 21));
        btn_dk->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0.392, x2:1, y2:0, stop:0 rgba(255, 85, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        btn_dk->setIconSize(QSize(128, 128));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(110, 160, 201, 16));
        label->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"font: italic 10pt \"Times New Roman\";"));
        DangNhap->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DangNhap);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        DangNhap->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DangNhap);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DangNhap->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DangNhap);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DangNhap->setStatusBar(statusBar);
        toolBar = new QToolBar(DangNhap);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        DangNhap->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(DangNhap);

        QMetaObject::connectSlotsByName(DangNhap);
    } // setupUi

    void retranslateUi(QMainWindow *DangNhap)
    {
        DangNhap->setWindowTitle(QApplication::translate("DangNhap", "Login to LIBPRO", Q_NULLPTR));
#ifndef QT_NO_WHATSTHIS
        DangNhap->setWhatsThis(QApplication::translate("DangNhap", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
#endif // QT_NO_WHATSTHIS
        btn_login->setText(QApplication::translate("DangNhap", "LOGIN", Q_NULLPTR));
        lbl_name->setText(QApplication::translate("DangNhap", "LIBPRO", Q_NULLPTR));
        lbl_note->setText(QApplication::translate("DangNhap", "Simple Library For Student", Q_NULLPTR));
        lbl_user->setText(QApplication::translate("DangNhap", "Username", Q_NULLPTR));
        lbl_pass->setText(QApplication::translate("DangNhap", "Password", Q_NULLPTR));
        btn_qmk->setText(QApplication::translate("DangNhap", "Qu\303\252n m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        btn_dk->setText(QApplication::translate("DangNhap", "\304\220\304\203ng k\303\275", Q_NULLPTR));
        label->setText(QApplication::translate("DangNhap", "Note", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("DangNhap", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DangNhap: public Ui_DangNhap {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DANGNHAP_H
