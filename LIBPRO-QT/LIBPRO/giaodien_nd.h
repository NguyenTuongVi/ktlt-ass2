#ifndef GIAODIEN_ND_H
#define GIAODIEN_ND_H

#include <QWidget>
#include <dangnhap.h>
#include <doimatkhau.h>

namespace Ui {
class Giaodien_ND;
}
class Giaodien_ND : public QWidget
{
    Q_OBJECT

public:
    explicit Giaodien_ND(QWidget *parent = 0);
    ~Giaodien_ND();

private slots:

    void on_btn_dangnhap_clicked();

    void on_btn_search_clicked();

    void on_btn_thoat_clicked();

    void on_btn_doimatkhau_clicked();

private:
    QWidget widget[4];
    Ui::Giaodien_ND *ui;
    DangNhap dn;
    DoiMatKhau dmk;
};

#endif // GIAODIEN_ND_H
