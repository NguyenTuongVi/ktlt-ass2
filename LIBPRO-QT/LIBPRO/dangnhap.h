#ifndef DANGNHAP_H
#define DANGNHAP_H

#include <QMainWindow>
#include "dangky.h"
#include "quen_mat_khau.h"

namespace Ui {
class DangNhap;
}
class DangNhap : public QMainWindow
{
    Q_OBJECT

public:
    explicit DangNhap(QWidget *parent = 0);
    ~DangNhap();

private slots:
    void on_toolButton_clicked();


    void on_btn_dk_clicked();

    void on_btn_qmk_clicked();

private:
    Ui::DangNhap *ui;
    DangKy dk;
    Quen_Mat_Khau qmk;
};


#endif // DANGNHAP_H
