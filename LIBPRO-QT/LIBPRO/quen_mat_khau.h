#ifndef QUEN_MAT_KHAU_H
#define QUEN_MAT_KHAU_H

#include <QWidget>

namespace Ui {
class Quen_Mat_Khau;
}

class Quen_Mat_Khau : public QWidget
{
    Q_OBJECT

public:
    explicit Quen_Mat_Khau(QWidget *parent = 0);
    ~Quen_Mat_Khau();

private:
    Ui::Quen_Mat_Khau *ui;
};

#endif // QUEN_MAT_KHAU_H
