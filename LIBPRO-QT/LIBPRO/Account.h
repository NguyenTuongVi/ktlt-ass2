#pragma once
#include<string>
using namespace std;
class Account
{
public:
	Account();
	string GetUsername();
	void SetUserName(string);
	string GetPassword();
	void SetPassword(string);
	string GetID();
	void SetID(string);
	bool GetState();
	void SetState(bool);
	~Account();
private:
	string Username;
	string Password;
	string ID;
	bool State;
};

