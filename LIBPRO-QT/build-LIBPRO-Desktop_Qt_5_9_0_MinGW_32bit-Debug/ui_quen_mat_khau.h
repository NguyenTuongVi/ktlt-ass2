/********************************************************************************
** Form generated from reading UI file 'quen_mat_khau.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QUEN_MAT_KHAU_H
#define UI_QUEN_MAT_KHAU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Quen_Mat_Khau
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QGroupBox *groupBox_2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *lineEdit_2;
    QLabel *label_5;
    QLineEdit *lineEdit_3;
    QPushButton *pushButton;
    QLabel *label_6;
    QLabel *label_7;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLabel *label_8;

    void setupUi(QWidget *Quen_Mat_Khau)
    {
        if (Quen_Mat_Khau->objectName().isEmpty())
            Quen_Mat_Khau->setObjectName(QStringLiteral("Quen_Mat_Khau"));
        Quen_Mat_Khau->resize(391, 300);
        groupBox = new QGroupBox(Quen_Mat_Khau);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 391, 81));
        groupBox->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(0, 85, 255, 255), stop:1 rgba(255, 255, 255, 255));\n"
"color: rgb(255, 255, 255);"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 20, 271, 31));
        label->setStyleSheet(QStringLiteral("font: 16pt \".VnArabiaH\";"));
        groupBox_2 = new QGroupBox(Quen_Mat_Khau);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 80, 391, 221));
        groupBox_2->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"font: 75 10pt \"Times New Roman\";"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 30, 61, 16));
        lineEdit = new QLineEdit(groupBox_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(140, 30, 113, 20));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 50, 47, 13));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 70, 111, 16));
        lineEdit_2 = new QLineEdit(groupBox_2);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(140, 70, 113, 20));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 100, 81, 21));
        lineEdit_3 = new QLineEdit(groupBox_2);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(140, 100, 113, 20));
        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(290, 190, 75, 23));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 130, 91, 16));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(30, 160, 101, 16));
        lineEdit_4 = new QLineEdit(groupBox_2);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(140, 130, 113, 20));
        lineEdit_5 = new QLineEdit(groupBox_2);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(140, 160, 113, 20));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(30, 190, 241, 21));
        label_8->setStyleSheet(QLatin1String("color: rgb(255, 0, 0);\n"
"font: italic 10pt \"Times New Roman\";"));

        retranslateUi(Quen_Mat_Khau);

        QMetaObject::connectSlotsByName(Quen_Mat_Khau);
    } // setupUi

    void retranslateUi(QWidget *Quen_Mat_Khau)
    {
        Quen_Mat_Khau->setWindowTitle(QApplication::translate("Quen_Mat_Khau", "Qu\303\252n m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("Quen_Mat_Khau", "LIBPRO", Q_NULLPTR));
        label->setText(QApplication::translate("Quen_Mat_Khau", "<html><head/><body><p>RESET PASSWORD <span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:sub;\">LIBPRO</span></p></body></html>", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("Quen_Mat_Khau", "RESET", Q_NULLPTR));
        label_2->setText(QApplication::translate("Quen_Mat_Khau", "H\341\273\215 v\303\240 t\303\252n", Q_NULLPTR));
        label_3->setText(QApplication::translate("Quen_Mat_Khau", "Or", Q_NULLPTR));
        label_4->setText(QApplication::translate("Quen_Mat_Khau", "CMND ho\341\272\267c MSSV", Q_NULLPTR));
        label_5->setText(QApplication::translate("Quen_Mat_Khau", "T\303\252n t\303\240i kho\341\272\243n", Q_NULLPTR));
        pushButton->setText(QApplication::translate("Quen_Mat_Khau", "RESET", Q_NULLPTR));
        label_6->setText(QApplication::translate("Quen_Mat_Khau", "M\341\272\255t kh\341\272\251u m\341\273\233i", Q_NULLPTR));
        label_7->setText(QApplication::translate("Quen_Mat_Khau", "Nh\341\272\255p l\341\272\241i m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        label_8->setText(QApplication::translate("Quen_Mat_Khau", "Note", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Quen_Mat_Khau: public Ui_Quen_Mat_Khau {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QUEN_MAT_KHAU_H
